package eneas.instantapp;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.firebase.client.Firebase;

import eneas.instantapp.dao.DaoMaster;
import eneas.instantapp.dao.SchemaDao;
import eneas.instantapp.services.DbService;
import eneas.instantapp.services.RestService;

/**
 * Created by Eneas on 29/11/2015.
 */
public class YourInstantApplication extends Application {

    public static Context appContext;
    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
        appContext = this;

    }

}