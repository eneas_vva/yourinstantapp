package eneas.instantapp.activities;


import android.app.ProgressDialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;

import eneas.instantapp.R;
import eneas.instantapp.YourInstantApplication;
import eneas.instantapp.adapters.AttributesAdapter;
import eneas.instantapp.dao.DaoMaster;
import eneas.instantapp.dao.DaoSession;
import eneas.instantapp.dto.Schema;
import eneas.instantapp.model.DataContent;
import eneas.instantapp.model.intercom.Field;
import eneas.instantapp.model.intercom.ListFields;
import eneas.instantapp.services.DbService;
import eneas.instantapp.services.RestService;

public class AttributesFragment extends Fragment {

    private static final String TAG = AttributesFragment.class.getName();
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private eneas.instantapp.dao.Schema schema;
    private ListFields<Field> fields;
    private LinearLayout llLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        schema = DataContent.getInstance().getApp_data().getSchema();

        llLayout = (LinearLayout) inflater.inflate(R.layout.activity_attributes, container, false);
        ((TextView)llLayout.findViewById(R.id.tablayout_attributes)).setText(schema.getTopic().toUpperCase());
        loadAttributes();

        return llLayout;

    }

    private void loadAttributes() {
        new AsyncTask<Void, Boolean, Boolean>() {
            ProgressDialog progDialog = new ProgressDialog(AttributesFragment.this.getActivity());
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Log.d(TAG, "Loading attributes from API");
                progDialog.setMessage("Loading attributes");
                progDialog.setIndeterminate(true);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(false);
                progDialog.show();
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    fields = RestService.getSchema(schema.getId());
                } catch (IOException e) {
                   return null;
                }
                return (fields != null);
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                progDialog.dismiss();
                if(result) {
                    AttributesUI();
                }
                else
                    ((RootActivity)getActivity()).nonCriticalError("I/O Error, try again.");

            }
        }.execute();

    }

    private void AttributesUI() {
        RecyclerView mRecyclerView = (RecyclerView)llLayout.findViewById(R.id.container_attributes);

        AttributesAdapter mAdapter = new AttributesAdapter(fields);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(llLayout.getContext()));
    }


}
