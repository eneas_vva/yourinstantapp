package eneas.instantapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import eneas.instantapp.R;
import eneas.instantapp.YourInstantApplication;
import eneas.instantapp.adapters.CategoriesAdapter;
import eneas.instantapp.app_gen.ListLayoutFragment;
import eneas.instantapp.app_gen.MapLayoutFragment;
import eneas.instantapp.model.BaseEntity;
import eneas.instantapp.model.CategoryEntity;
import eneas.instantapp.model.DataContent;
import eneas.instantapp.presenters.CategoriesPresenter;

public class CategoriesActivity extends RootActivity implements eneas.instantapp.views.CategoriesView {
    private static final String TAG = CategoriesActivity.class.getName();
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private CategoriesPresenter categoriesPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        categoriesPresenter = new CategoriesPresenter(this);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        refreshCategories(new OnDownloadedDataFromAPI() {
            @Override
            public void onFinish() {
                categoriesUI();
            }
        });


    }



    @Override
    @OnClick(R.id.categories_check_button)
    public void onClickCheckButton(View v) {
        if(categoriesPresenter.validateSelections()) {
            setResult(Activity.RESULT_OK);
            finish();
        }
        else {
            Snackbar.make(v, "No selected categories, choose at least one is mandatory", Snackbar.LENGTH_LONG).show();
        }
    }

    private void categoriesUI() {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
             return  DataContent.getInstance().getCategories().size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return DataContent.getInstance().getCategories().toArray(new CategoryEntity[0])[position].getTitle();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private RecyclerView mRecyclerView;
        private LinearLayoutManager mLayoutManager;
        private CategoriesAdapter mAdapter;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_categories, container, false);
            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.categories_list);
            mLayoutManager = new LinearLayoutManager(rootView.getContext());
            mRecyclerView.setLayoutManager(mLayoutManager);

            int category = getArguments().getInt(ARG_SECTION_NUMBER);
            List<CategoryEntity> categories = DataContent.getInstance().getCategories();
            mAdapter = new CategoriesAdapter( categories.toArray(new CategoryEntity[categories.size()])[category]);

            mRecyclerView.setAdapter(mAdapter);

            return rootView;
        }
    }
}
