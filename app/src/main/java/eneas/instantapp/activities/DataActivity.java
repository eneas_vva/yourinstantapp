package eneas.instantapp.activities;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.io.IOException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import eneas.instantapp.R;
import eneas.instantapp.dao.Repo;
import eneas.instantapp.dao.Schema;
import eneas.instantapp.model.DataContent;
import eneas.instantapp.presenters.DataPresenter;
import eneas.instantapp.services.RestService;
import eneas.instantapp.third_classes.Locator;
import eneas.instantapp.views.DataView;

public class DataActivity extends RootActivity implements
        DataView,
        OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        DataActivitySelectorRepoFragment.OnListFragmentInteractionListener,
        DataActivitySelectorSchemaFragment.OnListFragmentInteractionListener {


    private DataPresenter dataPresenter;


//    Location myLocation = getLastKnownLocation();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.data_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        dataPresenter = new DataPresenter(this);
        dataPresenter.setRadius(queryRadius());
        if(!getIntent().getBooleanExtra(DataPresenter.MULTIPOINT, false))
            dataPresenter.setMaxMarkers(1);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private int queryRadius() {
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        return Integer.valueOf(preferences.getString("radius", "1000"));
    }


    public void showRepositoriesDialog() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = DataActivitySelectorRepoFragment.newInstance(dataPresenter.repositoriesSelected());
        newFragment.show(ft, "dialog");

    }

    @Override
    @OnClick(R.id.data_check_button)
    public void onClickCheckButton(View v) {
        showRepositoriesDialog();
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        dataPresenter.setMap(googleMap);
        LocationManager mLocationManager;
/*        Location location = getLastKnownLocation();
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(longitude, latitude);*/
        googleMap.setMyLocationEnabled(true);

        Location location;
        new Locator(this).getLocation(Locator.Method.GPS, new Locator.Listener() {
            @Override
            public void onLocationFound(Location location) {
                LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 13));
            }

            @Override
            public void onLocationNotFound() {
            }
        });

        LatLng myLocation = new LatLng(39.4652765, -6.3724247);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 13));
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.setOnMapClickListener(this);
        googleMap.setOnMarkerClickListener(this);
        getSupportActionBar().show();
        //  mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in actual position"));
        //   mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        dataPresenter.loadMarkers();
    }

    @Override
    public void onListFragmentInteraction(final Repo repo) {
        dataPresenter.updateAppDataWithLocationsAndRepository(repo);


        showSchemasDialog(repo);
    }

    public void showSchemasDialog(final Repo repo) {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        new AsyncTask<Void, Boolean, Boolean>() {

            @Override
            protected Boolean doInBackground(Void... voids) {
                RestService restService = new RestService();
                try {
                    List<Schema> schemas = restService.getSchemasForRepository(repo.getId().intValue());
                    DataActivitySelectorSchemaFragment.newInstance(schemas).show(ft, "dialog");
                } catch(IOException e) {
                    return false;
                }
                return true;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                if(!result) {
                    nonCriticalError("Couldn't download schemas for this repository, try again later.");
                }
            }
        }.execute();
    }


    @Override
    public void onListFragmentInteraction(Schema schema) {
        dataPresenter.updateAppDataWithSchema(schema);
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        try {
            dataPresenter.addMarker(latLng);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        onMapClick(marker.getPosition());
        return dataPresenter.removeMarker(marker);
    }
}
