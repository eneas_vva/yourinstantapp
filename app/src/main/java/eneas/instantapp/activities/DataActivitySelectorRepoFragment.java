package eneas.instantapp.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import java.util.HashSet;
import java.util.Set;

import eneas.instantapp.dao.Repo;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class DataActivitySelectorRepoFragment extends DialogFragment {

    private Set<Repo> repos;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DataActivitySelectorRepoFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static DataActivitySelectorRepoFragment newInstance(HashSet<Repo> repos) {
        DataActivitySelectorRepoFragment fragment = new DataActivitySelectorRepoFragment();
        Bundle args = new Bundle();
        args.putSerializable("repos", repos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        repos = (HashSet<Repo>)getArguments().getSerializable("repos");

        ArrayAdapter<Repo> dataAdapter = new ArrayAdapter<Repo>(getActivity(), android.R.layout.simple_selectable_list_item, repos.toArray(new Repo[repos.size()]) );

        return new AlertDialog.Builder(getActivity())
                .setTitle("Select a concrete repository")
                .setSingleChoiceItems(dataAdapter, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        doPositiveClick((Repo) repos.toArray()[which]);
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                doNegativeClick();
                            }
                        }
                )
                .create();
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Repo answer);
    }

    public void doPositiveClick(Repo repo) {
        if(mListener != null)
            mListener.onListFragmentInteraction(repo);
        this.dismiss();
    }

    public void doNegativeClick() {
        this.dismiss();
    }
}
