package eneas.instantapp.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eneas.instantapp.R;
import eneas.instantapp.app_gen.AppgenActivity;
import eneas.instantapp.dao.Apps;
import eneas.instantapp.model.BaseEntity;
import eneas.instantapp.model.DataContent;
import eneas.instantapp.presenters.DataPresenter;
import eneas.instantapp.presenters.MainPresenter;
import eneas.instantapp.services.FirebaseService;

public class MainActivity extends RootActivity
        implements NavigationView.OnNavigationItemSelectedListener, eneas.instantapp.views.MainView
         {
    private static final String TAG = MainActivity.class.getName();

    public MainPresenter mainPresenter;


    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;

    // Contenedor para el paso 1: categorías
    @BindView(R.id.categories_item) RelativeLayout categories_item_container;
    // Contenedor para el paso 2: datos
    @BindView(R.id.data_item) RelativeLayout data_item_container;
        // Botones para el paso 2: datos
        @BindView(R.id.data_set_buttons) LinearLayout data_set_buttons;
    // Contenedor para el paso 3: atributos
    @BindView(R.id.attributes_item) LinearLayout  attributes_item_container;
        // Fragment que se carga dentro del contenedor de atributos, contiene el listado de cda uno de ellos.
        @BindView(R.id.attributes_fragment_container) LinearLayout attributes_fragment_container;
    // Contenedor para el paso 4: layout
    @BindView(R.id.layout_item) LinearLayout layout_item_container;
        // Botones para escoger la plantilla concreta que se quiere.
        @BindView(R.id.layout_set_buttons) LinearLayout layout_set_buttons;
    // Contenedor para el paso 5: name
    @BindView(R.id.name_item) LinearLayout name_item_container;
        // Botones para el check y la ayuda
        @BindView(R.id.content_main_name_options) RelativeLayout content_main_name_options;
        // Campo para escribir el nombre de la aplicación
        @BindView(R.id.name_editbox) EditText name_editbox;
    // Guardar aplicación
    @BindView(R.id.save_app_run) FloatingActionButton save_app_run;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainPresenter = new MainPresenter(this);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        navigationView.setNavigationItemSelectedListener(this);
        drawer.openDrawer(navigationView);

        populateNavigationDrawer();

    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        super.onAuthStateChanged(firebaseAuth);
        toggle.syncState();

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mainPresenter.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void enable2Step() {
        data_item_container.setAlpha(1); // Elimino transparencia
        categories_item_container.findViewWithTag("checked").setVisibility(View.VISIBLE);
        data_item_container.findViewWithTag("forbidden").setVisibility(View.INVISIBLE);
        data_set_buttons.setVisibility(View.VISIBLE);

        //((NestedScrollView)findViewById(R.id.main_scrollview_container)).scrollTo(0, container.getTop());
    }

    @Override
    public void enable3Step() {
        getSupportFragmentManager().beginTransaction().replace(R.id.attributes_fragment_container, new AttributesFragment()).commit();

        attributes_item_container.setAlpha(1); // Elimino transparencia
        data_set_buttons.setVisibility(View.GONE); // Minimizo el cjto. de opciones del data
        data_item_container.findViewWithTag("checked").setVisibility(View.VISIBLE);
        //data_set_buttons.findViewWithTag("checked").setVisibility(View.VISIBLE);
        attributes_item_container.findViewWithTag("forbidden").setVisibility(View.INVISIBLE);
        attributes_item_container.findViewById(R.id.content_main_attributes_options).setVisibility(View.VISIBLE);
        attributes_fragment_container.setVisibility(View.VISIBLE);
        // ((NestedScrollView)findViewById(R.id.main_scrollview_container)).scrollTo(0, attributes_item_container.getBottom());
    }

    @Override
    public void enable4Step() {
        layout_item_container.setAlpha(1); // Elimino transparencia
        attributes_fragment_container.setVisibility(View.GONE); // Hago desaparecer el cjto. de opciones del paso anterior
        layout_item_container.findViewWithTag("forbidden").setVisibility(View.INVISIBLE);
        // findViewById(R.id.categories_item).findViewWithTag("checked").setVisibility(View.VISIBLE);
        layout_set_buttons.setVisibility(View.VISIBLE);
        // ((NestedScrollView)findViewById(R.id.main_scrollview_container)).scrollTo(0, container.getTop());
    }

    @Override
    public void enable5Step() {
        name_item_container.setAlpha(1); // Elimino transparencia
        layout_set_buttons.setVisibility(View.GONE);// Hago desaparecer el cjto. de opciones del paso anterior
        layout_item_container.findViewWithTag("checked").setVisibility(View.VISIBLE);
        content_main_name_options.setVisibility(View.VISIBLE);
        name_item_container.findViewWithTag("forbidden").setVisibility(View.INVISIBLE);
        // findViewById(R.id.categories_item).findViewWithTag("checked").setVisibility(View.VISIBLE);
        name_editbox.setVisibility(View.VISIBLE);
        //     ((NestedScrollView)findViewById(R.id.main_scrollview_container)).scrollTo(0, container.getTop());

    }

    @Override
    public void finalStep() {
        name_editbox.setVisibility(View.GONE);
        name_item_container.findViewWithTag("checked").setVisibility(View.VISIBLE);
        save_app_run.setVisibility(View.VISIBLE);
        content_main_name_options.setVisibility(View.GONE);
    }

    @OnClick(R.id.categories_item)
    public void onClickCategoriesButton(View v) {
            Intent categories_section = new Intent(v.getContext(), CategoriesActivity.class);
            startActivityForResult(categories_section, MainPresenter.REQUEST_CATEGORIES_CODE);
    }

    @OnClick({R.id.data_set_buttons_manual, R.id.data_set_buttons_nearto, R.id.data_set_buttons_nearbetween, R.id.data_set_buttons_all})
    public void onClickDataButton(View v) {
        buttonEffect(v);
        mainPresenter.clickDataButton(v.getId());

    }

    @Override
    public int showLocationInput() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        EditText editText = new EditText(this);
        editText.setHint("Dirección");
        editText.setPadding(25,25,25,25);
        builder.setView(editText).setTitle("Choose city");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                enable3Step();
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO: Id de la ciudad elegida
            }
        });


        builder.show();

        return 0;
    }

    public void navigateToDataActivity(boolean multipoint) {
        Intent intent = new Intent(this, DataActivity.class);
        intent.putExtra(DataPresenter.MULTIPOINT, multipoint);
        startActivityForResult(intent, MainPresenter.RESULT_CODE_MAPSACTIVITY_LAUNCH);
    }


    @OnClick(R.id.attributes_check_button)
    public void onClickAttributesButton(View v) {
        findViewById(R.id.content_main_attributes_checked).setVisibility(View.VISIBLE);
        findViewById(R.id.content_main_attributes_options).setVisibility(View.GONE);
        mainPresenter.clickAttributesButton();
    }

    @OnClick({R.id.layout_set_buttons_list, R.id.layout_set_buttons_map})
    public void onClickLayoutButton(View v) {
        mainPresenter.clickLayoutButton(v.getId());
    }


    @OnClick(R.id.name_check_button)
    public void onClickNameButton(View v) {
        mainPresenter.clickNameButton(name_editbox.getText().toString());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
/*       if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        try {
            mainPresenter.NavigationItemSelected(item, new SyncAppsListenerForMainView() {
                @Override
                public void OnSyncCompleted() {
                    populateNavigationDrawer();
                }
            });
        } catch (FirebaseAuthException e) {
            e.printStackTrace();
        }

        return true;
    }


    public void populateNavigationDrawer() {
        List<Apps> list = mainPresenter.getListApps();
        navigationView.getMenu().clear();
        navigationView.getMenu().add(R.id.drawer_group_app, 1001, 0, "Sync");
        navigationView.getMenu().add(R.id.drawer_group_app, 1002, 0, "Delete local apps");
        navigationView.getMenu().add(R.id.drawer_group_app, 1003, 0, "Delete remote apps");
        for (final Apps app : list) {
            Log.e("app_id_2", ""+app.getApp_name());
            MenuItem item = navigationView.getMenu().add(R.id.drawer_group_app, app.getId().intValue(), 0, app.getApp_name())
                    .setIcon(
                            (app.getType().equals(BaseEntity.MAP))
                                    ? android.R.drawable.ic_menu_mapmode
                                    : android.R.drawable.ic_menu_agenda
                    );
            ImageButton removeButton = new ImageButton(this);
            removeButton.setImageDrawable(removeButton.getResources().getDrawable(R.drawable.ic_remove_circle_black_24dp));
            removeButton.setBackgroundColor(getResources().getColor(android.R.color.white));

            item.setActionView(removeButton);
            item.getActionView().setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    MainActivity.this.deleteApplicationDialog(app);
                    return true;
                }
            });
            ;
        }
    }

    private void deleteApplicationDialog(final Apps app) {
        final boolean[] checkedItems = { false };

        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage("Are you sure you want remove this app?")
                .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mainPresenter.deleteApp(app);
                        populateNavigationDrawer();
                    }
                })
                .setNeutralButton("FULL DELETE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            mainPresenter.deleteAppFull(app);
                            populateNavigationDrawer();
                        } catch(FirebaseAuthException e) {
                            nonCriticalError("Error of syncing, couldn't delete cloud app. Try again later.");
                        }
                    }
                })
                .setCancelable(true).create().show();
    }

    @Override
    public void launchAppGenerator(Long id, String app_data) {
        startActivity(new Intent(this, AppgenActivity.class).putExtra(MainPresenter.TAG_APP_SELECTED, app_data).putExtra(MainPresenter.TAG_APP_SELECTED_ID, id));
    }


    @OnClick(R.id.settings_button)
    public void onClickSettingsButton(View v) {
        switch(v.getId()) {
            case R.id.settings_button:
/*                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);*/
                 startActivityForResult(new Intent(this, SettingsActivity.class), MainPresenter.RC_SETTINGS);
        }
    }

    @OnClick(R.id.save_app_run)
    public void onClickSaveApp(View v) {
        Toast.makeText(v.getContext(), "Saving app ...", Toast.LENGTH_SHORT).show();
        mainPresenter.saveState(new SyncAppsListenerForMainView() {

            @Override
            public void OnSyncCompleted() {
                mainPresenter.syncCompleted();
            }
        });
    }

    public static void buttonEffect(View button){
        AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.4F);
        button.startAnimation(buttonClick);
    }
    public void restart() {
        Intent intent = MainActivity.this.getIntent();
        MainActivity.this.finish();
        MainActivity.this.startActivity(intent);

        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
    @Override
    protected void onDestroy() {
        DataContent.getInstance().remove();

        super.onDestroy();
    }

    public abstract class SyncAppsListenerForMainView implements FirebaseService.SyncAppsListener {
        @Override
        public void OnSyncError(KErrors error) {
            nonCriticalError("Couldn't sync apps, try again later.");
        }

    }
}
