package eneas.instantapp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;

import org.apache.commons.lang.ObjectUtils;

import java.io.IOException;

import eneas.instantapp.R;
import eneas.instantapp.YourInstantApplication;
import eneas.instantapp.model.DataContent;
import eneas.instantapp.presenters.MainPresenter;
import eneas.instantapp.services.DbService;
import eneas.instantapp.services.FirebaseService;
import eneas.instantapp.services.RestService;

/**
 * Created by Eneas on 14/08/2016.
 */
public class RootActivity extends AppCompatActivity implements FirebaseAuth.AuthStateListener {
    private static final String TAG = RootActivity.class.getName();
    boolean schemas_updated = false;
    boolean repos_updated = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateSchemas();
        auth();
    }

    private void auth() {
        if(FirebaseAuth.getInstance().getCurrentUser() == null) {
            Intent signInIntent = AuthUI.getInstance().createSignInIntentBuilder()
                    .setTheme(R.style.PurpleTheme)
                    .setLogo(R.drawable.logo_header)
                    .setProviders(AuthUI.GOOGLE_PROVIDER, AuthUI.FACEBOOK_PROVIDER, AuthUI.EMAIL_PROVIDER)
                    .setTosUrl(MainPresenter.GOOGLE_TOS_URL)
                    .build();

            startActivityForResult(
                    signInIntent,
                    MainPresenter.RC_GOOGLE_LOGIN);
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            mAuth.addAuthStateListener(this);
        }
    }

    public void updateSchemas() {
        if(!schemas_updated)
            new AsyncTask<Void, Void, Void>() {
                ProgressDialog progDialog = new ProgressDialog(RootActivity.this);
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    schemas_updated = true;
                    progDialog.setMessage("Updating LOCAL Schemas");
                    progDialog.setIndeterminate(true);
                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progDialog.setCancelable(false);
                    progDialog.show();
                }
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        DbService.getInstance().recycleSchemas(RestService.getSchemas());
                    } catch (NullPointerException e) { }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    progDialog.cancel();
                }
            }.execute();
    }

    public void updateRepos() {
        new AsyncTask<Void, Boolean, Boolean>() {
            ProgressDialog progDialog = new ProgressDialog(RootActivity.this);
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                repos_updated = true;
                progDialog.setMessage("Updating LOCAL Repositories");
                progDialog.setIndeterminate(true);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(false);
                progDialog.show();
            }
            @Override
            protected Boolean doInBackground(Void... params) {
                return DataContent.getInstance().getReposByNet();
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                progDialog.cancel();
                if(result == null) {
                    nonCriticalError();
                }
            }
        }.execute();
    }



    protected void refreshCategories(final OnDownloadedDataFromAPI onDownloadedDataFromAPI) {
        new AsyncTask<Void, Boolean, Boolean>() {
            ProgressDialog progDialog = new ProgressDialog(RootActivity.this);
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Log.d(TAG, "Downloading categories from API");
                progDialog.setMessage("Downloading categories");
                progDialog.setIndeterminate(true);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(false);
                progDialog.show();
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    return DataContent.getInstance().getCategoriesByNet();
                } catch (IOException e) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                progDialog.dismiss();

                if(result != null && result) {
                    onDownloadedDataFromAPI.onFinish();
                }
                else {
                    onDownloadedDataFromAPI.onError();
                }

            }
        }.execute();

    }


    private void nonCriticalError() {
        nonCriticalError("Couldn't complete this action, don't worry, try again.");
    }

    public void nonCriticalError(String message) {

        new AlertDialog.Builder(this)
                .setTitle("Non Critical Error")
                .setMessage(message)
                .setCancelable(true)
                .setNegativeButton("Continue", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick( DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public void criticalError(String message) {
        final DialogInterface.OnCancelListener cancelListener = new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                RootActivity.this.finish();
            }
        };

        new AlertDialog.Builder(this)
                .setTitle("Critical Error")
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent starterIntent = getIntent();
                        startActivity(starterIntent);
                        RootActivity.this.finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cancelListener.onCancel(dialog);
                    }
                })
                .setOnCancelListener(cancelListener)
                .create().show();
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        if( firebaseAuth.getCurrentUser() != null)
             FirebaseService.getInstance().retrieveFirebaseref(firebaseAuth);
    }

    abstract class OnDownloadedDataFromAPI {
        abstract void onFinish();
        void onError() {
            nonCriticalError("I/O Error, try again.");
        }
    }
}
