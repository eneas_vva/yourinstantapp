package eneas.instantapp.activities;

import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;

import eneas.instantapp.R;
import eneas.instantapp.presenters.MainPresenter;

public class SettingsActivity extends PreferenceActivity {

    public static int RC_REQUIRE_RESTART_APP = 20001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference_settings);
        PreferenceScreen loggedAs = (PreferenceScreen)findPreference("logged_as");
        if(FirebaseAuth.getInstance().getCurrentUser() != null) { // Si el usuario está logueado
            loggedAs.setIcon(getResources().getDrawable(R.drawable.common_plus_signin_btn_icon_dark)); // Se muestra el icono para login
            loggedAs.setSummary(FirebaseAuth.getInstance().getCurrentUser().getDisplayName()); // Se le muestra el nombre con el que está
            loggedAs.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) { // Si hace click sobre él
                    FirebaseAuth.getInstance().signOut(); // Se le desautentifica
                    setResult(SettingsActivity.RC_REQUIRE_RESTART_APP);
                    finish(); // Se finaliza la actividad para forzar la reautentificación.
                    return true;
                }
            });
        }
        else { // En caso de no estar logueado
            loggedAs.setIcon(getResources().getDrawable(R.drawable.common_plus_signin_btn_icon_dark));  // Se muestra botón para login
            loggedAs.setSummary("Not logged in"); // En lugar del nombre del usuario aparece que no está logueado
            loggedAs.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) { // Si hace click
                    finish(); // Se fuerza la autentificación
                    return true;
                }
            });
        }
    }

}
