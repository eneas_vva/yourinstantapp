package eneas.instantapp.adapters;

/**
 * Created by Eneas on 10/10/2015.
 */

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import eneas.instantapp.R;
import eneas.instantapp.dto.Attribute;
import eneas.instantapp.dto.Schema;
import eneas.instantapp.model.BaseEntity;
import eneas.instantapp.model.DataContent;
import eneas.instantapp.model.intercom.Field;
import eneas.instantapp.model.intercom.ListFields;

/**
 * Created by Eneas on 10/10/2015.
 */
public class AttributesAdapter extends RecyclerView.Adapter<AttributesAdapter.ViewHolder> {
    private static final String TAG = AttributesAdapter.class.getName();
    private ListFields<Field> attributes;
    private BaseEntity mAppData = DataContent.getInstance().getApp_data();
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTextView;
        LinearLayout mLinearLayout;

        public ViewHolder(LinearLayout v) {
            super(v);
            mLinearLayout = (LinearLayout) v.findViewById(R.id.activity_attributes_listview_items_layout);
            mTextView = (TextView)v.findViewById(R.id.activity_attributes_listview_items_textview);
            mTextView.setClickable(true);

        }

        public TextView getmTextView() {
            return mTextView;
        }

        public LinearLayout getmLinearLayout() {
            return mLinearLayout;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AttributesAdapter(ListFields<Field> attributes) {

        this.attributes = attributes;
        for(Field attribute : attributes) {
            Log.d(TAG, attribute.getField());
            if( attribute.getField() == null ||attribute.getField().isEmpty() )
                attributes.remove(attribute);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AttributesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_attributes_listview_items, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder((LinearLayout)v.findViewById(R.id.activity_attributes_listview_items_layout));

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String item_tmp;
        TextView mTextView = holder.getmTextView();
        LinearLayout mLinearLayout = holder.getmLinearLayout();
        switch(position) {
            case 0: item_tmp = attributes.getField_id(); break;
            case 1: item_tmp = attributes.getField_title(); break;
            case 2: item_tmp = attributes.getField_geolocation(); break;
            default:
                item_tmp = attributes.getOrdinary_fields().get(position-3).getField();
                mTextView.setCompoundDrawables(null,null,null,null);
                final String item = item_tmp;
                if(mAppData.isSelectedAttribute(item))
                    mTextView.setBackground(null);
                mTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!mAppData.isSelectedAttribute(item)) {
                            v.setBackground(null);
                            mAppData.selectAttributes(item);
                        }
                        else {
                            v.setBackgroundResource(android.R.drawable.editbox_dropdown_light_frame);
                            mAppData.unselectAttributes(item);
                        }

                    }
                });
        }
        mTextView.setText(item_tmp);


    }

    // Return the size of your dataset (invoked by the layout manager),
    @Override
    public int getItemCount() {
        return attributes.size()-1;
    }

}

