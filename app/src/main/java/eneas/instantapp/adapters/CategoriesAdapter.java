package eneas.instantapp.adapters;

/**
 * Created by Eneas on 10/10/2015.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import eneas.instantapp.R;
import eneas.instantapp.model.BaseEntity;
import eneas.instantapp.model.CategoryEntity;
import eneas.instantapp.model.CategoryEntityItem;
import eneas.instantapp.model.DataContent;

/**
 * Created by Eneas on 10/10/2015.
 */
public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {
    private List<CategoryEntityItem> keywords;
    private BaseEntity mAppData = DataContent.getInstance().getApp_data();

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {


        // each data item is just a string in this case
        private TextView mTextView;

        public ViewHolder(LinearLayout v) {
            super(v);
            mTextView = (TextView)v.findViewById(R.id.activity_categories_listview_textview);
            mTextView.setClickable(true);
        }

        public TextView getTextView() {
            return mTextView;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CategoriesAdapter(CategoryEntity category) {
        this.keywords = category.getKeywords();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_categories_listview_items, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder((LinearLayout)v.findViewById(R.id.content_main_layout_item));

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int id) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        TextView mTextView = holder.getTextView();
        mTextView.setText(keywords.get(id).getKeyword());
        if(mAppData.getCategories().contains(keywords.get(id)))
            mTextView.setBackgroundResource(android.R.drawable.editbox_dropdown_light_frame);
        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( ! mAppData.getCategories().contains(keywords.get(id))) {
                    v.setBackgroundResource(android.R.drawable.editbox_dropdown_light_frame);
                    mAppData.getCategories().add(keywords.get(id));
                }
                else {
                    v.setBackground(null);
                    mAppData.getCategories().remove(keywords.get(id));
                }

            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager),
    @Override
    public int getItemCount() {
       //Log.e("count", "Categoría_"+category+" "+mAppData.getCategories().get(category).getItems().size());
        return keywords.size();
    }

}

