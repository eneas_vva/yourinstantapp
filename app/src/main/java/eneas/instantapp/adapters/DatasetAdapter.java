package eneas.instantapp.adapters;

/**
 * Created by Eneas on 10/10/2015.
 */

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import eneas.instantapp.R;
import eneas.instantapp.app_gen.AppgenActivity;
import eneas.instantapp.app_gen.Utils;
import eneas.instantapp.model.intercom.AbstractModel;

/**
 * Created by Eneas on 10/10/2015.
 */
public class DatasetAdapter extends RecyclerView.Adapter<DatasetAdapter.ViewHolder> {

    private AppgenActivity mContext;
    private List<AbstractModel> mAppData;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView mTextViewLabel;
        private TextView mTextViewAddress;
        private CardView cardView;
        private int position;

        public ViewHolder(CardView v) {
            super(v);
            mTextViewLabel = (TextView) v.findViewById(R.id.appgen_label);
            mTextViewAddress = (TextView) v.findViewById(R.id.appgen_address);
            cardView = v;
        }

        public void setVisibility(boolean isVisible){
            RecyclerView.LayoutParams param = (RecyclerView.LayoutParams)itemView.getLayoutParams();
            if (isVisible){
                param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                param.width = LinearLayout.LayoutParams.MATCH_PARENT;
                itemView.setVisibility(View.VISIBLE);
            }else{
                itemView.setVisibility(View.GONE);
                param.height = 0;
                param.width = 0;
            }
            itemView.setLayoutParams(param);
        }
/*        public void setPosition(int position) {
        }*/
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public DatasetAdapter(AppgenActivity context, List<AbstractModel> appData) {
        mAppData =appData;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DatasetAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_appgen_listview_items, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder((CardView)v.findViewById(R.id.activity_appgen_listview_item));

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        final AbstractModel element = mAppData.get(position);
        try {
            holder.mTextViewLabel.setText(element.getName());
            holder.mTextViewAddress.setText(Utils.getAddressString(mContext, element.getGeolocation().get(0), element.getGeolocation().get(1), element.getGeolocation().toString()));
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.launchApp(element);
                }
            });
        } catch (Exception e) {
            holder.setVisibility(false);
        }

    }

    // Return the size of your dataset (invoked by the layout manager),
    @Override
    public int getItemCount() {
       //Log.e("count", "Categoría_"+category+" "+mAppData.getCategories().get(category).getItems().size());
        return mAppData.size(); // Cantidad de datos del opendata
    }
    public void setFilter(List<AbstractModel> appData) {
        mAppData = new ArrayList<>();
        mAppData.addAll(appData);
        notifyDataSetChanged();
    }
}

