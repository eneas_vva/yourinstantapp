package eneas.instantapp.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import eneas.instantapp.R;
import eneas.instantapp.app_gen.DetailsFragment.OnListFragmentInteractionListener;
import eneas.instantapp.model.intercom.AbstractModel;
import eneas.instantapp.model.intercom.SchemaValue;

import java.util.ArrayList;
import java.util.List;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.ViewHolder> {
    private final AbstractModel mValues;
    private final OnListFragmentInteractionListener mListener;

    public DetailsAdapter(AbstractModel datamodel, OnListFragmentInteractionListener listener) {
        mValues = datamodel;
        mListener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.details_fragment_attribute, parent, false);
        return new ViewHolder(view);
    }

    private void setCardView(ViewHolder holder, String id, String content) {

        if(content != null && !content.isEmpty()) {
            holder.mIdView.setText(id);
            holder.mContentView.setText(content);
        }
        else {
            holder.setVisibility(false);
        }
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        switch(position) {
            case 0: setCardView(holder, "Id", mValues.getId()); break;
            case 1: setCardView(holder, "Geolocation", mValues.getGeolocation().toString()); break;
            default:
                position = position-2;
                setCardView(holder, mValues.getVars().keySet().toArray(new String[0])[position], mValues.getVars().values().toArray(new SchemaValue[0])[position].getValue());
        }


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.getVars().size() + 2; // Valores variables + estáticos
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final CardView mCardView;
        public SchemaValue mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            mCardView = (CardView)view.findViewById(R.id.cardview);
        }

        public void setVisibility(boolean isVisible){
            RecyclerView.LayoutParams param = (RecyclerView.LayoutParams)itemView.getLayoutParams();
            if (isVisible){
                param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                param.width = LinearLayout.LayoutParams.MATCH_PARENT;
                itemView.setVisibility(View.VISIBLE);
            }else{
                itemView.setVisibility(View.GONE);
                param.height = 0;
                param.width = 0;
            }
            itemView.setLayoutParams(param);
        }
        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
