package eneas.instantapp.app_gen;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import eneas.instantapp.R;
import eneas.instantapp.activities.RootActivity;
import eneas.instantapp.dao.Schema;
import eneas.instantapp.model.BaseEntity;
import eneas.instantapp.model.DataContent;
import eneas.instantapp.model.intercom.AbstractModel;
import eneas.instantapp.presenters.MainPresenter;
import eneas.instantapp.services.DbService;
import eneas.instantapp.services.OpenDataService;

public class AppgenActivity extends RootActivity {

    interface KErrors extends OpenDataService.KErrors {
        int RC_NOGEOLOCATIONS = 10;
    }
    private ListLayoutFragment mListLayoutFragment;
    private static final String TAG = AppgenActivity.class.getName();
    private BaseEntity appinfo;
    private MapLayoutFragment mMapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appgen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.appgen_activity_toolbar);
        setSupportActionBar(toolbar);
        Bundle bundle = getIntent().getExtras();
        appinfo = (new Gson()).fromJson(bundle.getString(MainPresenter.TAG_APP_SELECTED), BaseEntity.class);
        appinfo.setApp_Id( bundle.getLong(MainPresenter.TAG_APP_SELECTED_ID) );
        getSupportActionBar().setTitle(appinfo.getApp_name() );


        new AsyncTask<Void, Integer, Integer>() {
            ProgressDialog progDialog = new ProgressDialog(AppgenActivity.this);
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Log.d("APP", "Arrancando la app: " + appinfo.getApp_name() + " - Type: " + appinfo.getApp_Id()+ " - " + appinfo.getLayout_type());
                progDialog.setMessage("Downloading dataset");
                progDialog.setIndeterminate(true);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(false);
                progDialog.show();
            }

            @Override
            protected Integer doInBackground(Void... params) {
                boolean result = false;
                try {
                    result = DataContent.getInstance().getDatasetByNet(appinfo.getRepositoryId(), appinfo.getScchemaId());
                    if( DataContent.getInstance().getDataset().isEmpty() )
                        return OpenDataService.KErrors.RC_NORESULTS;
                    if (appinfo.getLayout_type().equals(BaseEntity.MAP) && result) {
                        if(hasGeolocations(DataContent.getInstance().getDataset()))
                            showMapFragment();
                        else
                            return KErrors.RC_NOGEOLOCATIONS;
                    } else if (appinfo.getLayout_type().equals(BaseEntity.LIST) && result) {
                            showListFragment();
                    }

                } catch(IOException | NullPointerException e) {}
                return ( result ) ? OpenDataService.KErrors.RC_OKAY : OpenDataService.KErrors.RC_CONNECTION_PROBLEMS;

            }

            @Override
            protected void onPostExecute(Integer result) {
                super.onPostExecute(result);
                progDialog.dismiss();
                switch (result) {
                    case KErrors.RC_CONNECTION_PROBLEMS:
                        criticalError("Couldn't execute generated app because of a connection problem. Try again.");
                        break;
                    case KErrors.RC_NORESULTS:
                        criticalError("There are no results.");
                        break;
                    case KErrors.RC_NOGEOLOCATIONS:
                        criticalError("No results with geolocations, use list layout.");
                }

            }
        }.execute();

    }

    private void showListFragment() {
        mListLayoutFragment = ListLayoutFragment.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.appgen_fragment_container, mListLayoutFragment).commit();

    }

    private void showMapFragment() {
        mMapFragment = MapLayoutFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.appgen_fragment_container, mMapFragment).commit();
    }

    private boolean hasGeolocations(List<AbstractModel> dataset) {
        for (AbstractModel abstractModel : dataset) {
            if(!abstractModel.getGeolocation().isEmpty()) return true;
        }
        return false;
    }

    public void launchRawView(AbstractModel abstractModel) {
        DetailsFragment detailsFragment = DetailsFragment.newInstance(abstractModel);
        detailsFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void launchApp(final AbstractModel abstractModel) {


        final Schema schema = DbService.getInstance().getSchemaById(appinfo.getScchemaId());
        Log.d(TAG, "Launching detailsActivity for " + abstractModel.getName() + " using schema " + schema.getTopic());
        final Intent intent = getPackageManager().getLaunchIntentForPackage(schema.getAndroid_package());
            AlertDialog warning_app_not_installed = new AlertDialog.Builder(this)
                    .setTitle("Choose action")
                    .setMessage("There aren't specific APP yet, but you can view destructured data.")
                    .setPositiveButton("View data", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            launchRawView(abstractModel);
                            return;
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .setCancelable(true)

                    .create();
            if(schema.getAndroid_package() != null && !schema.getAndroid_package().isEmpty()) {

                if (intent == null) {
                    warning_app_not_installed .setMessage("We've found a specific app");

                    warning_app_not_installed.setButton(DialogInterface.BUTTON_NEUTRAL, "Download APP", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Utils.downloadAndExecuteAPK(AppgenActivity.this, schema.getAndroid_package());
                            return;
                        }
                    });
                } else {
                    warning_app_not_installed .setMessage("You've installed a specific app");
                    warning_app_not_installed.setButton(DialogInterface.BUTTON_NEUTRAL, "Run APP", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            intent.putExtra("model", abstractModel);
                            startActivity(intent);
                            return;
                        }
                    });
                }
            }
            warning_app_not_installed.show();


    }


}
