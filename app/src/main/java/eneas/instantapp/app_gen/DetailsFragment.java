package eneas.instantapp.app_gen;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import eneas.instantapp.R;
import eneas.instantapp.adapters.DetailsAdapter;
import eneas.instantapp.model.intercom.AbstractModel;
import eneas.instantapp.model.intercom.SchemaValue;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class DetailsFragment extends DialogFragment {

    // TODO: Customize parameter argument names
    private static final String ARG_DATAMODEL = "datamodel";
    private OnListFragmentInteractionListener mListener;
    private AbstractModel datamodel;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DetailsFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static DetailsFragment newInstance(AbstractModel abstractModel) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_DATAMODEL, abstractModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            datamodel = (AbstractModel) getArguments().getSerializable(ARG_DATAMODEL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_fragment_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new DetailsAdapter(datamodel, mListener));
        }

        getDialog().setTitle(datamodel.getName());
        TextView title = (TextView)getDialog().findViewById( android.R.id.title );
        ViewGroup.LayoutParams params = title.getLayoutParams();
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
        title.setPadding(20, 20, 20, 20);
        title.setTextColor( getResources().getColor( R.color.colorPrimary ) );

        getDialog().setCancelable(true);


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
           // throw new RuntimeException(context.toString()
              //      + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(SchemaValue item);
    }
}
