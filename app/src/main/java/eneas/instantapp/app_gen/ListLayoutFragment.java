package eneas.instantapp.app_gen;

import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import eneas.instantapp.R;
import eneas.instantapp.adapters.DatasetAdapter;
import eneas.instantapp.model.DataContent;
import eneas.instantapp.model.intercom.AbstractModel;
import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

public class ListLayoutFragment extends Fragment implements SearchView.OnQueryTextListener  {
    private LinearLayoutManager mLayoutManager;
    private DatasetAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private List<AbstractModel> mAppData;
    // TODO: Rename parameter arguments, choose names that match




    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ListLayoutFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListLayoutFragment newInstance() {
        ListLayoutFragment fragment = new ListLayoutFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    public ListLayoutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RelativeLayout llLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_appgen_list_layout, container, false);;
setHasOptionsMenu(true);
        mRecyclerView = (RecyclerView) llLayout.findViewById(R.id.appgen_rv);
        setRecyclerViewLayoutManager(mRecyclerView);
        mAppData =  DataContent.getInstance().getDataset();
        mAdapter = new DatasetAdapter((AppgenActivity)getActivity(), mAppData);

        mRecyclerView.setAdapter(mAdapter);
        return llLayout;
    }
    /**
     * Set RecyclerView's LayoutManager
     */
    public void setRecyclerViewLayoutManager(RecyclerView recyclerView) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (recyclerView.getLayoutManager() != null) {
            scrollPosition =
                    ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.scrollToPosition(scrollPosition);
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_appgen_list, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        mAdapter.setFilter(mAppData);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        final List<AbstractModel> filteredModelList = filter(mAppData, query);
        mAdapter.setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
    private List<AbstractModel> filter(List<AbstractModel> models, String query) {
        query = query.toLowerCase();

        final List<AbstractModel> filteredModelList = new ArrayList<>();
        for (AbstractModel model : models) {
            final String text = model.getName().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
}
