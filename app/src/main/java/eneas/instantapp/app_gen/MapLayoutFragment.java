package eneas.instantapp.app_gen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Hashtable;

import eneas.instantapp.model.DataContent;
import eneas.instantapp.model.intercom.AbstractModel;

public class MapLayoutFragment extends SupportMapFragment implements OnMapReadyCallback {


    private GoogleMap mMap;
    private Hashtable<Marker, AbstractModel> map_makers = new Hashtable<>();
    private ArrayList<Double> mLat = new ArrayList<Double>();
    private ArrayList<Double> mLong = new ArrayList<Double>();
    public MapLayoutFragment(){
        super();
    }

    public static MapLayoutFragment newInstance() {
        MapLayoutFragment fragment = new MapLayoutFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater,container,savedInstanceState);
        getMapAsync(this);
/*        Fragment fragment = getParentFragment();
        if (fragment != null && fragment instanceof SupportMapFragment) {
            ((SupportMapFragment) fragment).getMapAsync(this);
        }*/
        //mMap = getMap();        //Obtengo el mapa.


        return view;
    }

    private void addMarkersToMap() {
        for(AbstractModel elem : DataContent.getInstance().getDataset()) {
            if(elem.getGeolocation().size() == 2) { // Si no se tiene geolocalización del elemento no se muestra (solución fácil)
                mLat.add(elem.getGeolocation().get(0));
                mLong.add(elem.getGeolocation().get(1));

                MarkerOptions mkoptions = new MarkerOptions().position(new LatLng(mLat.get(mLat.size() - 1), mLong.get(mLong.size() - 1))).title(elem.getName()).snippet("Touch for detailed view");
                Marker marker = mMap.addMarker(mkoptions);
                map_makers.put(marker, elem);
            }
        }
    }

    public void inicializarMapa(){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(median(mLat), median(mLong)), 13));   //Mueve la camara a CÃ¡ceres, Zoom de 13.
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);     //Tipo de mapa: Hybrid.
        UiSettings settings = mMap.getUiSettings();     //Obtengo las opciones del mapa.
        settings.setZoomControlsEnabled(true);          //Activo los botones de zoom.
        settings.setMyLocationButtonEnabled(true);      //Activo el botÃ³n de posiciÃ³n propia.
        settings.setZoomGesturesEnabled(true);          //Activo el zoom con los dedos.
        settings.setRotateGesturesEnabled(false);       //Desactivo la rotaciÃ³n del mapa con los dedos.
        settings.setMapToolbarEnabled(true);
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                AbstractModel abstractModel = map_makers.get(marker);
                ((AppgenActivity)getActivity()).launchApp(abstractModel);

            }
        });
    }

    public static double median(ArrayList<Double> m) {
        int middle = m.size()/2;
        if (m.size()%2 == 1) {
            return m.get(middle);
        } else {
            return (m.get(middle-1) + m.get(middle)) / 2.0;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        addMarkersToMap();    //AÃ±adimos las ubicaciones en el mapa.
        inicializarMapa();      //Inicializo vista y opciones
    }

 /*   @Override
    public void onMapReady(final GoogleMap map) {

        map.setMyLocationEnabled(true);

        Location location = map.getMyLocation();
        if(location == null)
            location = getLastKnownLocation();
        if(location == null) {
            GPSTracker mGPS = new GPSTracker(this);
            location = mGPS.getLocation();

        }
        LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15));

        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        loadMarkers(map);
    }

    private void loadMarkers(GoogleMap map) {
        Log.d("sad", "test");
        for(Binding elem : DataContent.getInstance().getDataset()) {
            map.addMarker(new MarkerOptions().position(new LatLng(Float.valueOf(elem.getGeoLat().getValue()), Float.valueOf(elem.getGeoLong().getValue()))).title(elem.getRdfsLabel().getValue()));

        }

    }
    private Location getLastKnownLocation() {
        LocationManager mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }*/
    /**
     * Listener interface to tell when the map is ready
     */
/*    public static interface OnMapReadyListener {

        void onMapReady();
    }*/
}
