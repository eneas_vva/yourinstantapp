package eneas.instantapp.app_gen;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import eneas.instantapp.R;

/**
 * Created by Eneas on 06/08/2016.
 */
public class Utils {
    private static Geocoder geocoder;
    private static HashMap<LatLng, String> coordinatesAddresses = null;
    private static final String TAG = ConnectionUtilities.class.getName();

    public static void downloadAndExecuteAPK(final Activity context, String fileName) {
        //get destination to update file and set Uri
        //TODO: First I wanted to store my update .apk file on internal storage for my app but apparently android does not allow you to open and install
        //aplication with existing package from there. So for me, alternative solution is Download directory in external storage. If there is better
        //solution, please inform us in comment
        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        destination += fileName;
        final Uri uri = Uri.parse("file://" + destination);
        //Delete update file if exists
        File file = new File(destination);
        if (file.exists())
            //file.delete() - test this, I think sometimes it doesnt work
            file.delete();

        //get url of app on server
//        String url = "https://cdn-cf.winudf.com/c/APK/1734/023d3e27c90dae25.apk?_fn=UG9rw6ltb24gR09fdjAuMzEuMF9hcGtwdXJlLmNvbS5hcGs&_p=Y29tLm5pYW50aWNsYWJzLnBva2Vtb25nbw%3D%3D&c=2%7CGAME_ADVENTURE&k=417e0bd3493b30ecd0e0a99f06e8f1ca57a97171";

        //set downloadmanager
        String url = "http://diy-eneas.rhcloud.com/yourinstantapp/web/apk/" + fileName;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        // request.setDescription(context.getString(R.string.notification_description));
        request.setTitle(context.getString(R.string.app_name));

        //set destination
        request.setDestinationUri(uri);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);

        //set BroadcastReceiver to install app when .apk is downloaded
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            @Override

            public void onReceive(Context ctxt, Intent intent) {
                Intent install = new Intent(Intent.ACTION_VIEW);
                install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                install.setDataAndType(uri,
                        "application/vnd.android.package-archive");
                context.startActivity(install);
                context.unregisterReceiver(this);
                context.finish();
            }
        };
        //register receiver for when .apk download is compete
        context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public static String getAddressString(Context context, double latitude, double longitude,
                                          String defaultReturn) {

        LatLng coordinates = new LatLng(latitude,longitude);

        if(geocoder == null)
            geocoder = new Geocoder(context, Locale.getDefault());
        if(coordinatesAddresses == null)
            coordinatesAddresses =  new HashMap<LatLng, String>();

        // If address string has been already obtained, we just return it
        if(coordinatesAddresses.containsKey(coordinates))
            return coordinatesAddresses.get(coordinates);

        // We try to retrieve the address using Geocoder
        String address = getAddressStringUsingGeocode(latitude, longitude, coordinates);
        if(address != null && !address.equals(""))
            return address;

        // If Geocoder does not not work, we try to retrieve the address through google MAPS API
        address = getAddressStringUsingGoogleMapsAPI(
                context, latitude, longitude, address, coordinates);
        if(address != null && !address.equals(""))
            return address;

        // Once here, nothing worked to retrieve the address
        return defaultReturn;
    }

    private static String getAddressStringUsingGeocode(
            double latitude, double longitude, LatLng coordinates) {
        String address = "";

        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

            if(addresses != null) {
                if(!addresses.isEmpty()) {
                    Address a = addresses.get(0);
                    address += a.getAddressLine(0);
                    address += (a.getLocality() != null) ? (", " + a.getLocality()) : "";
                    //address += (a.getCountryName() != null) ? (", " + a.getCountryName()) : "";
                    coordinatesAddresses.put(coordinates, address);
                }
            }
        } catch (IOException e) {
            Log.e(TAG,
                    "Error getting address from coordinates: " + e.getMessage());
        }
        return address;
    }

    private static String getAddressStringUsingGoogleMapsAPI(
            Context context,
            double latitude,
            double longitude,
            String address,
            LatLng coordinates) {

        if (ConnectionUtilities.available(context)) {
            String result = null;
            String url = null;

            try {
                url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="
                        + URLEncoder.encode(String.valueOf(latitude), "UTF-8") + ","
                        + URLEncoder.encode(String.valueOf(longitude), "UTF-8")
                        + "&sensor=true";
            } catch (UnsupportedEncodingException e) {
                // Ignore
            }

            result = ConnectionUtilities.getJSonData(url);
            if(result != null) {
                JSONObject jObject = null;

                try {
                    jObject = new JSONObject(result);
                    JSONArray jArray = jObject.getJSONArray("results");
                    JSONObject jAddress = (JSONObject) jArray.get(0);
                    address = jAddress.getString("formatted_address");

                    if(address != null && !address.equals("")) {
                        coordinatesAddresses.put(coordinates, address);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "Error reading JSON: " + e.getMessage());
                }
            }
        }
        return address;
    }
}
