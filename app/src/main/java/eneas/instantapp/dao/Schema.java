package eneas.instantapp.dao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "SCHEMA".
 */
public class Schema {

    private Long id;
    private String topic;
    private Long timestamp;
    /** Not-null value. */
    private String schema;
    private String android_package;

    public Schema() {
    }

    public Schema(Long id) {
        this.id = id;
    }

    public Schema(Long id, String topic, Long timestamp, String schema, String android_package) {
        this.id = id;
        this.topic = topic;
        this.timestamp = timestamp;
        this.schema = schema;
        this.android_package = android_package;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    /** Not-null value. */
    public String getSchema() {
        return schema;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getAndroid_package() {
        return android_package;
    }

    public void setAndroid_package(String android_package) {
        this.android_package = android_package;
    }
    @Override
    public String toString() {
        return getTopic();
    }
}
