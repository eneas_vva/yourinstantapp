
        package eneas.instantapp.dto;

        import com.google.gson.JsonObject;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class Mapping {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("schema")
    @Expose
    private Integer schema;
    @SerializedName("dataset")
    @Expose
    private Integer dataset;
    @SerializedName("metadata")
    @Expose
    private JsonObject metadata;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The schema
     */
    public Integer getSchema() {
        return schema;
    }

    /**
     *
     * @param schema
     * The schema
     */
    public void setSchema(Integer schema) {
        this.schema = schema;
    }

    /**
     *
     * @return
     * The dataset
     */
    public Integer getDataset() {
        return dataset;
    }

    /**
     *
     * @param dataset
     * The dataset
     */
    public void setDataset(Integer dataset) {
        this.dataset = dataset;
    }

    /**
     *
     * @return
     * The metadata
     */
    public JsonObject getMetadata() {
        return metadata;
    }

    /**
     *
     * @param metadata
     * The metadata
     */
    public void setMetadata(JsonObject metadata) {
        this.metadata = metadata;
    }

}