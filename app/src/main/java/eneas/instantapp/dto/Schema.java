package eneas.instantapp.dto;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Schema {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("topic")
    @Expose
    private String topic;
    @SerializedName("last_modified")
    @Expose
    private long lastModified;
    @SerializedName("schema")
    @Expose
    private List<Attribute> schema = new ArrayList<Attribute>();
    @SerializedName("android_package")
    @Expose
    private String android_package;


    private Schema() {

    }

    public eneas.instantapp.dao.Schema getSchemaDaoCopy() {
        eneas.instantapp.dao.Schema output = new eneas.instantapp.dao.Schema();
        eneas.instantapp.dto.Schema input = this;
        output.setId(input.getId());
        output.setTimestamp(input.getLastModified());
        output.setTopic(input.getTopic());
        output.setSchema((new Gson()).toJson(input.getSchema()));
        output.setAndroid_package(input.getAndroid_package());

        return output;
    }

    static public Schema newInstance(eneas.instantapp.dao.Schema input) {
        Schema output = new Schema();
        output.setId(input.getId());
        output.setLastModified(input.getTimestamp());
        output.setTopic(input.getTopic());
        Attribute [] attribs = (new Gson()).fromJson(input.getSchema(), Attribute[].class);
        output.setSchema(Arrays.asList(attribs));

        return output;
    }


    /**
     *
     * @return
     * The id
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     *
     * @param topic
     * The topic
     */
    public void setTopic(String topic) {
        this.topic = topic;
    }

    /**
     *
     * @return
     * The lastModified
     */
    public long getLastModified() {
        return lastModified;
    }

    /**
     *
     * @param lastModified
     * The last_modified
     */
    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    /**
     *
     * @return
     * The schema
     */
    public List<Attribute> getSchema() {
        return schema;
    }

    /**
     *
     * @param schema
     * The schema
     */
    public void setSchema(List<Attribute> schema) {
        this.schema = schema;
    }

    public String getAndroid_package() {
        return android_package;
    }

    public void setAndroid_package(String android_package) {
        this.android_package = android_package;
    }


}