package eneas.instantapp.model;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.Vector;

import eneas.instantapp.dao.Apps;
import eneas.instantapp.dao.Repo;
import eneas.instantapp.dao.Schema;

/**
 * Created by Eneas on 10/10/2015.
 */
public class BaseEntity {

    public static final String LIST = "list";
    public static final String MAP = "map";
    Set<Integer> selected_categories = new HashSet<Integer>();
    @Expose(serialize = true)
    Set<String> selected_attributes = new HashSet<String>();
    Vector<LatLng> selected_markers = new Vector<LatLng>();
    HashSet<CategoryEntityItem> categories = new HashSet<>();
    @SerializedName("id")
    @Expose(serialize = true)
    private Long app_id;
    @Expose(serialize = true)
    private String layout_type = LIST;
    @Expose(serialize = true)
    private String app_name;
    @Expose(serialize = true)
    private Long repository_id;

    private Repo repository;

    @Expose(serialize = true)
    private Long schema_id;

    private Schema schema;


    public HashSet<CategoryEntityItem> getCategories() {
        return categories;
    }


    public void selectAttributes(String keyword) {
        selected_attributes.add(keyword);
    }

    public void unselectAttributes(String keyword) {
        selected_attributes.remove(keyword);
    }

    public boolean isSelectedAttribute(String keyword) {
        return selected_attributes.contains(keyword);
    }

    public BaseEntity setLocations(Set<Marker> markers) {
        for(Marker marker : markers) {
            selected_markers.add(marker.getPosition());
        }
        return this;
    }

    public Long getApp_Id() { return app_id; }

    public void setApp_Id(Long id) { this.app_id = id; }

    public String getLayout_type() {
        return layout_type;
    }

    public void setLayout_type(String layout_type) {
        this.layout_type = layout_type;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public Apps toDao() {
            Apps app = new Apps();
            app.setApp_name(app_name);
            app.setType(layout_type);
            GsonBuilder gson = new GsonBuilder();
            gson.excludeFieldsWithoutExposeAnnotation();
            gson.serializeNulls();

            String data = gson.create().toJson(this);
            app.setData(data);
            Log.w("data", data);
            return app;
    }

    public BaseEntity fromDao(Apps app) {
        BaseEntity savedInstance =  new Gson().fromJson(app.getData(), BaseEntity.class);

        return savedInstance;
    }

    public Repo getRepository() {
        return repository;
    }

    public void setRepository(Repo repository) {
        this.repository = repository;
        repository_id = repository.getId();
        Log.e("test", ""+repository_id);
    }

    public Long getRepositoryId() { return repository_id; }

    public Schema getSchema() {
        return schema;
    }
    public void setSchema(Schema schema) {
        schema_id = schema.getId();
        this.schema = schema;
    }

    public Long getScchemaId() { return schema_id; }


    public boolean isSelectedCategoriesEmpty() {
        return getCategories().isEmpty();
    }

    public void clean() {
        selected_categories.clear();
        selected_attributes.clear();
        selected_markers.clear();
    }


}
