
package eneas.instantapp.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryEntity {

    @SerializedName("name")
    @Expose
    private String title;
    @SerializedName("children")
    @Expose
    private List<CategoryEntityItem> keywords = new ArrayList<>();


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<CategoryEntityItem> getKeywords() {
        return keywords;
    }

    public void setKeyword(List<CategoryEntityItem> keywords) {
        this.keywords = keywords;
    }
}
