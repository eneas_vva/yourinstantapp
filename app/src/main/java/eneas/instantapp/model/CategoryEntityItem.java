
package eneas.instantapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryEntityItem {

    @SerializedName("name")
    @Expose
    private String keyword;

    /**
     * 
     * @return
     *     The keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * 
     * @param keyword
     *     The keyword
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryEntityItem that = (CategoryEntityItem) o;

        return keyword.equals(that.keyword);

    }

    @Override
    public int hashCode() {
        return keyword.hashCode();
    }
}
