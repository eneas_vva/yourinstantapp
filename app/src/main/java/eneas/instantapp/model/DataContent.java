package eneas.instantapp.model;

import android.content.Context;
import android.support.annotation.WorkerThread;

import java.io.IOException;
import java.util.List;

import eneas.instantapp.YourInstantApplication;
import eneas.instantapp.dao.Repo;
import eneas.instantapp.model.intercom.AbstractModel;
import eneas.instantapp.services.DbService;
import eneas.instantapp.services.RestService;

/**
 * Created by Eneas on 30/10/2015.
 */
public class DataContent {
    private static DataContent ourInstance = new DataContent();
    public static final String DATABASE_NAME = "YourInstantApp";


    private BaseEntity app_data = new BaseEntity();


    private List<CategoryEntity> categories;
    private List<AbstractModel> dataset;

    public static DataContent getInstance() {
        return ourInstance;
    }

    private DataContent() { }


    public boolean getReposByNet() {
        List<Repo> repos = RestService.getRepositories();
        if(repos != null) {
            DbService.getInstance().recycleRepos(repos);
            return true;
        }
        return false;

    }

    @WorkerThread
    public boolean getCategoriesByNet() throws IOException {
        categories = RestService.getCategories();
        return categories != null;
    }

    @WorkerThread
    public boolean getDatasetByNet(long id_repo, long id_schema) throws IOException {
        dataset = RestService.getListAbstractModelUsingRepoAndSchema(id_repo, id_schema);

        return dataset != null;
    }

    public BaseEntity getApp_data() {
        return app_data;
    }
    public List<AbstractModel> getDataset() {
        return dataset;
    }
    public DataContent remove() {
        getApp_data().clean();
        return ourInstance = this;
    }

    public List<CategoryEntity> getCategories() {
        return categories;
    }
}
