package eneas.instantapp.model.intercom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Eneas on 07/08/2016.
 */
public class AbstractModel implements Serializable {
    private String id;
    private String name;
    private List<Double> geolocation = new ArrayList<>();
    private Hashtable<String, SchemaValue> vars;
    static final long serialVersionUID =1224861857559450833L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Hashtable<String, SchemaValue> getVars() {
        return vars;
    }

    public void setVars(Hashtable<String, SchemaValue> vars) {
        this.vars = vars;
    }

    public List<Double> getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(List<Double> geolocation) {
        this.geolocation = geolocation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
