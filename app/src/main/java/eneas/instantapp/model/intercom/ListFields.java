package eneas.instantapp.model.intercom;

import java.util.ArrayList;

/**
 * Created by Eneas on 07/08/2016.
 */
public class ListFields<E extends Field> extends ArrayList<Field> {
    private String field_id;
    private String field_title;
    private String field_geolocation;
    private ArrayList<Field> ordinary_fields = new ArrayList<>();

    @Override
    public boolean add(Field field) {
        boolean result = super.add(field);
        if(field.getType().equals("title")) field_title = field.getField();
        else if(field.getType().equals("geolocation")) field_geolocation = field.getField();
        else if(field.getType().equals("pkey")) field_id = field.getField();
        else ordinary_fields.add(field);
        return result;
    }

    public String getField_id() {
        return field_id;
    }

    public String getField_geolocation() {
        return field_geolocation;
    }

    public String getField_title() {
        return field_title;
    }

    public ArrayList<Field> getOrdinary_fields() {
        return ordinary_fields;
    }

}
