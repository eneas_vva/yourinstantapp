package eneas.instantapp.model.intercom;

import java.io.Serializable;

/**
 * Created by Eneas on 07/08/2016.
 */
public class SchemaValue implements Serializable {
    private String type;
    private String value;
    static final long serialVersionUID =1224861857559250833L;

    public SchemaValue(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
