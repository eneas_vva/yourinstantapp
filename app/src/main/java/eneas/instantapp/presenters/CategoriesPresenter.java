package eneas.instantapp.presenters;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;

import eneas.instantapp.activities.CategoriesActivity;
import eneas.instantapp.model.DataContent;
import eneas.instantapp.views.CategoriesView;

/**
 * Created by Eneas on 27/08/2016.
 */
public class CategoriesPresenter {
    private final CategoriesView categoriesView;

    public CategoriesPresenter(CategoriesView categoriesView) {
        this.categoriesView = categoriesView;
    }


    public boolean validateSelections() {
        return !DataContent.getInstance().getApp_data().isSelectedCategoriesEmpty();

    }
}
