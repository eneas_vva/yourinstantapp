package eneas.instantapp.presenters;

import android.app.Activity;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import eneas.instantapp.R;
import eneas.instantapp.YourInstantApplication;
import eneas.instantapp.activities.RootActivity;
import eneas.instantapp.dao.Repo;
import eneas.instantapp.dao.Schema;
import eneas.instantapp.model.DataContent;
import eneas.instantapp.services.DbService;
import eneas.instantapp.views.DataView;

/**
 * Created by Eneas on 27/08/2016.
 */
public class DataPresenter  {
    private DataView dataView;
    private HashSet<Marker> markers = new HashSet<>();
    private Map<Integer, Circle> circles = new TreeMap<>();
    private Map<Repo, LatLng> repo_positions = new TreeMap<>();
    private int maxMarkers = 10;
    private GoogleMap mMap;
    public static final String MULTIPOINT = "MAP_W_MULTIPOINT_CHOICE";
    private int radius = 1000;

    public DataPresenter(DataView dataView) {
        this.dataView = dataView;
        if(dataView instanceof RootActivity)
            ((RootActivity)dataView).updateRepos();
        else
            throw new RuntimeException("View must extends RootActivity");
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }


    private Location getLastKnownLocation() {
        LocationManager mLocationManager = (LocationManager) YourInstantApplication.appContext.getSystemService(Activity.LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    public void setMaxMarkers(int maxMarkers) {
        this.maxMarkers = maxMarkers;
    }
    

    public void loadMarkers() {
        List<Repo> repos = DbService.getInstance().getReposByKeyword(DataContent.getInstance().getApp_data().getCategories());
        for(Repo schema : repos) {
            MarkerOptions marker = new MarkerOptions();
            LatLng pos = new LatLng(schema.getGeo_lat(), schema.getGeo_long());
            marker.position(pos);
            BitmapDescriptor bmDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.gmap_flag_repo_available);
            marker.icon(bmDescriptor);
            marker.zIndex(-1000.0f);
            mMap.addMarker(marker);
            repo_positions.put(schema, pos);

        }
    }



    public HashSet<Repo> repositoriesSelected() {
        HashSet<Repo> result = new HashSet<>();
        for(Map.Entry<Integer, Circle> circle : circles.entrySet()) {
            for(Map.Entry<Repo, LatLng> repo_pos : repo_positions.entrySet()) {
                float[] results = new float[1];
                Location.distanceBetween(repo_pos.getValue().latitude, repo_pos.getValue().longitude, circle.getValue().getCenter().latitude, circle.getValue().getCenter().longitude, results);
                if(results[0] <= radius) {
                    result.add(repo_pos.getKey());
                }
            }
        }

        return result;
    }



    public void addMarker(LatLng latLng) throws Exception {
        if (maxMarkers > 0) {
            boolean flag_some_marker_is_near = false;
            for(Marker marker : markers) {
                double x[] = { latLng.latitude, marker.getPosition().latitude };
                double y[] = { latLng.longitude, marker.getPosition().longitude }; /*
                double distance = Math.sqrt(Math.pow((x[0]*x[0] - x[1]*x[1]), 2) + Math.pow((y[0]*y[0] - y[1]*y[1]), 2)); // distancia de euclides - nofunciona el radio está en otra medida
   */
                float[] results = new float[1];
                Location.distanceBetween(x[0], y[0], x[1], y[1], results);
                if(results[0] <= radius) flag_some_marker_is_near = true;
            }
            if(flag_some_marker_is_near) return;

            MarkerOptions actual_position = new MarkerOptions();
            actual_position.position(latLng);
            actual_position.zIndex(1000.0f);
            Marker marker = mMap.addMarker(actual_position);
            markers.add(marker);
            circles.put(
                    marker.hashCode(), // Utilizamos el código hash del marcador como elemento índice
                    mMap.addCircle(new CircleOptions().center(latLng).radius(radius).strokeColor(Color.argb(80, 0, 0, 255)).fillColor(Color.argb(50, 0, 0, 255 )))
            );
            maxMarkers--;
            throw new Exception("In order to delete a marker just click over it");
        } else
            throw new Exception("Maximum number of markers exceeded, you must come back");

    }

    public boolean removeMarker(Marker marker) {
        if(markers.contains(marker))
        {
            marker.remove();
            circles.get(marker.hashCode()).remove();
            markers.remove(marker);
            maxMarkers++;
        }
        return false;
    }

    public void setMap(GoogleMap map) {
        this.mMap = map;
    }

    public void updateAppDataWithLocationsAndRepository(Repo repo) {
        DataContent.getInstance().getApp_data().setLocations(markers);
        DataContent.getInstance().getApp_data().setRepository(repo);
    }

    public void updateAppDataWithSchema(Schema schema) {
        DataContent.getInstance().getApp_data().setSchema(schema);
    }
}
