package eneas.instantapp.presenters;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.gms.auth.api.Auth;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;

import java.util.List;

import eneas.instantapp.R;
import eneas.instantapp.activities.MainActivity;
import eneas.instantapp.activities.SettingsActivity;
import eneas.instantapp.dao.Apps;
import eneas.instantapp.model.BaseEntity;
import eneas.instantapp.model.DataContent;
import eneas.instantapp.repositories.AppRepository;
import eneas.instantapp.services.DataSource;
import eneas.instantapp.services.FirebaseService;
import eneas.instantapp.views.MainView;

/**
 * Created by Eneas on 21/06/2016.
 */
public class MainPresenter {
    public final static int REQUEST_CATEGORIES_CODE =           10001;
    public final static int RESULT_CODE_MAPSACTIVITY_LAUNCH =   10002;
    public final static int RC_GOOGLE_LOGIN =                   10003;
    public static final int RC_SETTINGS =               10004;
    public final static String TAG_APP_SELECTED =              "APP_SELECTED";
    public final static String TAG_APP_SELECTED_ID =           "APP_SELECTED_ID" ;
    public static final String GOOGLE_TOS_URL =
            "https://www.google.com/policies/terms/";
    private static final String TAG = MainPresenter.class.getName();

    MainView mainView;
    public FirebaseService firebaseService = FirebaseService.getInstance();
    public AppRepository appRepository = new AppRepository();

    public MainPresenter(MainView mainView) {
        this.mainView = mainView;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RC_SETTINGS && resultCode == SettingsActivity.RC_REQUIRE_RESTART_APP) {
            mainView.restart();
        }
        if(resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CATEGORIES_CODE:
                    mainView.enable2Step();
                    break;
                case RESULT_CODE_MAPSACTIVITY_LAUNCH:
                    mainView.enable3Step();
                    break;
            }
        }
    }

    public void clickDataButton(int id) {
        switch(id) {
            case R.id.data_set_buttons_manual:
                mainView.showLocationInput();
                break;
            case R.id.data_set_buttons_nearto:
                mainView.navigateToDataActivity(false);
                break;
            case R.id.data_set_buttons_nearbetween:
                mainView.navigateToDataActivity(true);
                break;
            case R.id.data_set_buttons_all:
                mainView.enable3Step();
                break;
        }
    }

    public void clickAttributesButton() {
        mainView.enable4Step();
    }

    public void clickLayoutButton(int id) {
        switch(id) {
            case R.id.layout_set_buttons_map:
                DataContent.getInstance().getApp_data().setLayout_type(BaseEntity.MAP);
                break;
            case R.id.layout_set_buttons_list:
                DataContent.getInstance().getApp_data().setLayout_type(BaseEntity.LIST);
                break;
        }

        mainView.enable5Step();
    }

    public void clickNameButton(String app_name) {
        DataContent.getInstance().getApp_data().setApp_name(app_name);
        mainView.finalStep();
    }

    public void NavigationItemSelected(MenuItem item, MainActivity.SyncAppsListenerForMainView syncAppsListenerForMainView) throws FirebaseAuthException {

        switch(item.getItemId()) {
            case 1001:
                Log.d(TAG, "Starting to sync");
                firebaseService.sync(syncAppsListenerForMainView);
            break;
        case 1002:
            Log.d(TAG, "Deleting all local apps");
            appRepository.deleteAll();
            mainView.populateNavigationDrawer();
            break;
        case 1003:
            Log.d(TAG, "Deleting all remote apps");
            firebaseService.deleteApps();
            break;
        default:
                Apps app_selected = appRepository.getApp((String) item.getTitle());
                String app_data = app_selected.getData();
            Log.e("app_id", ""+item.getItemId());
                mainView.launchAppGenerator((long)item.getItemId(), app_data);

            //closeDrawer(GravityCompat.START);
        }
    }

    public void deleteApp(Apps app) {
        appRepository.delete(app);
    }


    public void deleteAppFull(Apps app) throws FirebaseAuthException {
        FirebaseService.getInstance().deleteApp(app);

        deleteApp(app);
    }


    public void saveState(final MainActivity.SyncAppsListenerForMainView syncAppsListenerForMainView) {
        appRepository.saveState(new DataSource.CallbackAfterSaveState() {

            @Override
            public void onPostExecute() {
                FirebaseService.getInstance().sync(syncAppsListenerForMainView);

            }
        });
    }

    public void syncCompleted() {
        mainView.restart();
    }

    public List<Apps> getListApps() {
        return appRepository.getListApps();
    }
}
