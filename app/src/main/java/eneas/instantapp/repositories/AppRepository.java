package eneas.instantapp.repositories;

import java.util.List;

import eneas.instantapp.dao.Apps;
import eneas.instantapp.services.DataSource;
import eneas.instantapp.services.FirebaseService;

/**
 * Created by Eneas on 26/06/2016.
 */

public class AppRepository extends Repository {
    public static AppRepository instance;
    public AppRepository() {
        super();
    }

    public static AppRepository getInstance() {
        if (instance == null)
            instance = new AppRepository();
        return instance;
    }

    public List<Apps> getListApps() {
        return dataSource.getListApps();
    }

    public Apps getApp(String app_name) {
        return dataSource.getApp(app_name);
    }

    public void saveState(DataSource.CallbackAfterSaveState callback) {
        dataSource.saveState(callback);
    }


    public void downloadApp(Apps app_cloud) { dataSource.downloadApp(app_cloud);}

    public void delete(Apps app) { dataSource.deleteApp(app);  }

    public void deleteAll() { dataSource.deleteAll();  }
}
