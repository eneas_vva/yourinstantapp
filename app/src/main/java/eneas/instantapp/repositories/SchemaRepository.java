package eneas.instantapp.repositories;

import eneas.instantapp.dao.Schema;

/**
 * Created by Eneas on 21/06/2016.
 */
public class SchemaRepository extends Repository{
    public SchemaRepository() {
        super();
    }

    public Schema getSchemaById(Long schema_id) {
        return dataSource.getSchemaById(schema_id);
    }

}
