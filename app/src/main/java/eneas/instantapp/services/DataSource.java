package eneas.instantapp.services;

import java.util.HashSet;
import java.util.List;

import eneas.instantapp.dao.Apps;
import eneas.instantapp.dao.Repo;
import eneas.instantapp.dao.Schema;
import eneas.instantapp.model.CategoryEntityItem;

/**
 * Created by Eneas on 26/06/2016.
 */
public interface DataSource {

    public List<Apps> getListApps();

    public Apps getApp(String app_name);

    public void recycleSchemas(List<Schema> schemas);

    public void recycleRepos(List<Repo> repos);

    public List<Repo> getReposByKeyword(HashSet<CategoryEntityItem> categories);

    public void deleteApp(Apps app);

    public void saveState(CallbackAfterSaveState callback);

    public void downloadApp(Apps app_cloud);

    public Schema getSchemaById(Long schema_id);

    public void deleteAll();

    public interface CallbackAfterSaveState {
        public void onPostExecute();
    }
}
