package eneas.instantapp.services;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import de.greenrobot.dao.query.WhereCondition;
import eneas.instantapp.YourInstantApplication;
import eneas.instantapp.dao.Apps;
import eneas.instantapp.dao.AppsDao;
import eneas.instantapp.dao.DaoMaster;
import eneas.instantapp.dao.DaoSession;
import eneas.instantapp.dao.Repo;
import eneas.instantapp.dao.RepoDao;
import eneas.instantapp.dao.RepoDao.Properties;
import eneas.instantapp.dao.Schema;
import eneas.instantapp.dao.SchemaDao;
import eneas.instantapp.model.CategoryEntityItem;
import eneas.instantapp.model.DataContent;

public class DbService implements DataSource {
    private static DbService instance;
    // Binder given to clients
    private DaoSession daoSession;
    private Context context;
    List<Apps> apps_list_cloud = new LinkedList();

    public DbService() {
        this.context = YourInstantApplication.appContext;
    }

    public static DataSource getInstance() {
        if(instance==null)
        {
            instance = new DbService();
        }
        return instance;
    }

    // Conecta con la base de datos y obtiene una sesión
    public DaoSession getSession() {
        if(daoSession == null) {
            SQLiteDatabase db = new DaoMaster.DevOpenHelper(context, DataContent.DATABASE_NAME, null).getWritableDatabase();
            DaoMaster daoMaster = new DaoMaster(db);
            daoSession = daoMaster.newSession();
            AppsDao appDao = daoSession.getAppsDao();
        }

        return daoSession;
    }


// Crea una aplicación salvando el estado

    public void saveState(final CallbackAfterSaveState callback) {

        new AsyncTask<Integer, Integer, Integer>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Integer doInBackground(Integer... params) {
                DaoSession daoSession = getSession();
                AppsDao appDao = daoSession.getAppsDao();

                Apps app = DataContent.getInstance().getApp_data().toDao();
                appDao.insert(app);
                //firebaseService.upload_app(app);

                return null;
            }

            @Override
            protected void onPostExecute(Integer integer) {
                super.onPostExecute(integer);
                callback.onPostExecute();
            }
        }.execute();

    }

    public void downloadApp(Apps app_cloud) {
        getSession().insert(app_cloud);
    }

    @Override
    public void deleteAll() {
        getSession().deleteAll(Apps.class);
    }

    @Override
    public List<Apps> getListApps() {
        List<Apps> list = getSession().getAppsDao().queryBuilder().orderDesc(AppsDao.Properties.App_name).list();
        Log.e("called", list.toString());

        return list;
    }

    @Override
    public Apps getApp(String app_name) {
        AppsDao appDao = daoSession.getAppsDao();
        Apps app_selected = appDao.queryBuilder().where(AppsDao.Properties.App_name.eq(app_name)).unique();

        return app_selected;
    }

    @Override
    public Schema getSchemaById(Long schema_id) {
        SchemaDao schemasDao = daoSession.getSchemaDao();

        return schemasDao.load(schema_id);
    }

    @Override
    public void recycleSchemas(List<Schema> schemas) {
        for(Schema schema : schemas) {
            daoSession.insertOrReplace(schema);
        }
    }

    @Override
    public void recycleRepos(List<Repo> repos) {
        daoSession.getRepoDao().deleteAll();
        for(Repo repo : repos) {
            daoSession.insert(repo);
        }
    }

    @Override
    public List<Repo> getReposByKeyword(HashSet<CategoryEntityItem> categories) {
        List<WhereCondition> whereConditions = new ArrayList<>();
        for(CategoryEntityItem category : categories) {
            whereConditions.add(Properties.Keywords.like("%" + category.getKeyword() +"%"));
        }
        List<Repo> repo;
        switch(whereConditions.size()) {
            case 1:
                repo = daoSession.getRepoDao().queryBuilder().where(whereConditions.get(0)).list();
                break;
            case 2:
                repo = daoSession.getRepoDao().queryBuilder().whereOr(whereConditions.get(0), whereConditions.get(1)).list();
                break;
            default:
                repo = daoSession.getRepoDao().queryBuilder().whereOr(whereConditions.get(0), whereConditions.get(1), whereConditions.subList(2, 0).toArray(new WhereCondition[0])).list();
        }

        return repo;
    }


    @Override
    public void deleteApp(Apps app) {
        daoSession.getAppsDao().delete(app);
    }

}