package eneas.instantapp.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.firebase.client.FirebaseError;
import com.firebase.ui.auth.util.CredentialsAPI;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import eneas.instantapp.YourInstantApplication;
import eneas.instantapp.dao.Apps;
import eneas.instantapp.repositories.AppRepository;

/**
 * Created by Eneas on 23/06/2016.
 */
public class FirebaseService {
    Context appContext = YourInstantApplication.appContext;

    private static FirebaseService instance = null;
    private AppRepository appRepository = AppRepository.getInstance();
    private DatabaseReference myFirebaseRef;
    ;

    private FirebaseService()
    {
        //retrieve_firebaseref();
    }

    public static FirebaseService getInstance()
    {
        if(instance==null)
        {
            instance = new FirebaseService();
        }
        return instance;
    }

    private boolean IsDataPersistedOnCloud() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(appContext);
        return pref.getBoolean("persistence_trough_cloud", false);
    }

    public DatabaseReference retrieveFirebaseref(FirebaseAuth firebaseAuth) {
        return myFirebaseRef = FirebaseDatabase.getInstance().getReference().child(firebaseAuth.getCurrentUser().getUid());

    }
    public DatabaseReference useFirebaseref() throws FirebaseAuthException {
        if(myFirebaseRef == null)
            myFirebaseRef = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        if(myFirebaseRef == null) {
            throw new FirebaseAuthException("AuthError", "Couldn't auth with firebase");
        }
        return myFirebaseRef;
    }
    public <T> List<T> intersection(List<T> list1, List<T> list2) {
        List<T> list = new ArrayList<T>();

        for (T t : list1) {
            if(list2.contains(t)) {
                list.add(t);
            }
        }

        return list;
    }



    public void checkdiffs(Apps app_cloud) {
        if(appRepository.getApp(app_cloud.getApp_name()) == null) {
            Log.d("APP", "Nueva app en la nube: " + app_cloud.getApp_name());
            appRepository.downloadApp(app_cloud);
        }

    }


    public void upload_app(Apps app_local) throws FirebaseAuthException {
    //    retrieve_firebaseref(MainActivity.mGoogleToken);
        useFirebaseref().child("apps").child(app_local.getApp_name()).setValue(app_local);
    }

    public void deleteApps() throws FirebaseAuthException {
    //    retrieve_firebaseref(MainActivity.mGoogleToken);
        useFirebaseref().child("apps").removeValue();
    }

    public void deleteApp(Apps app) throws FirebaseAuthException {
        useFirebaseref().child("apps").child(app.getApp_name()).removeValue();
    }

    public void sync(final SyncAppsListener syncAppsListener) {
        try {
            if (IsDataPersistedOnCloud()) { // Si el usuario quiere persistir los datos en la nube
                useFirebaseref().child("apps").addListenerForSingleValueEvent(new ValueEventListener() { // Se agrega un listener a las apps
                    @Override
                    public void onDataChange(DataSnapshot appsSnapshot) { // Por cada diferencia de dato
                        for (DataSnapshot app : appsSnapshot.getChildren()) // Se obtiene la descripción de la aplicación
                            checkdiffs(app.getValue(Apps.class)); // Se comprueba la diferencia, si no existe en local se descarga
                        syncAppsListener.OnSyncCompleted(); // Una vez se hayan comprobado las diferencias notificamos al caller
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        syncAppsListener.OnSyncError(SyncAppsListener.KErrors.CANCELLED); // Si ha habido algún error se lo notificamos al caller

                    }
                });
                List<Apps> apps_list_local = appRepository.getListApps(); // Se obtiene la lista de aplicaciones locales
                for (final Apps app : apps_list_local) { // Por cada aplicación local
                    useFirebaseref().child("apps").child(app.getApp_name()).addListenerForSingleValueEvent(new ValueEventListener() {  // Se comprueba si existe en la nube
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) { // Se saca una imagen de los datos
                            if (!dataSnapshot.exists()) try { // Si no existe imagen del dato es porque no existe la app en la nube
                                upload_app(app); // Como no existe se sube
                            } catch (FirebaseAuthException e) {
                                syncAppsListener.OnSyncError(SyncAppsListener.KErrors.UNAUTH); // Si ha habido error subiendo se notifica al caller
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            syncAppsListener.OnSyncError(SyncAppsListener.KErrors.CANCELLED); // Si ha habido error accediendo a la bd se notifica al caller

                        }


                    });
                }
            }
        } catch (FirebaseAuthException e) {
            syncAppsListener.OnSyncError(SyncAppsListener.KErrors.UNAUTH);
        }
    }

    public interface SyncAppsListener {
        enum KErrors { CANCELLED, UNAUTH }
        public void OnSyncCompleted();

        void OnSyncError(KErrors error);
    }
}
