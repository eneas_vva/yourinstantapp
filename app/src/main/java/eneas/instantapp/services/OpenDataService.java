package eneas.instantapp.services;

import java.util.List;

import eneas.instantapp.dao.Schema;
import eneas.instantapp.dto.Mapping;
import eneas.instantapp.dto.Repo;
import eneas.instantapp.model.CategoryEntity;
import eneas.instantapp.model.intercom.Field;
import eneas.instantapp.model.intercom.ListFields;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Consulta:
 * select ?URI ?rdfs_label ?schema_streetAddress ?geo_lat ?geo_long where{
     ?URI a om:Restaurante.
     ?URI rdfs:label ?rdfs_label.
     ?URI schema:address ?schema_address
     OPTIONAL { ?schema_address schema:streetAddress ?schema_streetAddress.}
     OPTIONAL { ?URI geo:lat ?geo_lat.}
     OPTIONAL { ?URI geo:long ?geo_long.}
 }
 */


public interface OpenDataService {
    interface KErrors {
        int RC_NORESULTS = 0;
        int RC_OKAY = 1;
        int RC_CONNECTION_PROBLEMS = 2;
    }

    @GET("/yourinstantapp/web/app_dev.php/mapping/repo/{id_repo}/schema/{id_schema}/read/json")
    Call<Object> getDataset(@Path("id_repo") long id_repo, @Path("id_schema") long id_schema);

    @GET("/yourinstantapp/web/app_dev.php/mapping/{id}/read/json")
    Call<Object> getDataset(@Path("id") long id );

    @GET("/yourinstantapp/web/app_dev.php/repo/all")
    Call<List<Repo>> getRepos();

    @GET("/yourinstantapp/web/app_dev.php/mapping/dataset/{id_repo}")
    Call<List<Mapping>> getMappings(@Path("id_repo") long id_repo);

    @GET("/yourinstantapp/web/app_dev.php/schemator/json/{id}")
    Call<ListFields<Field>> getAttributes(@Path("id") long id );

    @GET("/yourinstantapp/web/app_dev.php/schemator/all")
    Call<List<eneas.instantapp.dto.Schema>> getSchemas();

    @GET("/yourinstantapp/web/app_dev.php/categories/json/")
    Call<List<CategoryEntity>> getCategories();

/*    @GET("/yourinstantapp/web/app_dev.php/schemator/all")
    Call<List<Schema>> getAttributes();*/

}

