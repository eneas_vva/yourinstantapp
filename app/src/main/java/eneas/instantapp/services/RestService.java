package eneas.instantapp.services;

import android.support.annotation.WorkerThread;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import eneas.instantapp.YourInstantApplication;
import eneas.instantapp.dao.Repo;
import eneas.instantapp.dao.Schema;
import eneas.instantapp.dto.Mapping;
import eneas.instantapp.model.CategoryEntity;
import eneas.instantapp.model.intercom.AbstractModel;
import eneas.instantapp.model.intercom.Field;
import eneas.instantapp.model.intercom.ListFields;
import eneas.instantapp.model.intercom.SchemaValue;
import eneas.instantapp.repositories.SchemaRepository;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by a67281303 on 16/7/16.
 */

@WorkerThread
public class RestService {
    private final static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://diy-eneas.rhcloud.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Deprecated
    public static List<Schema> getSchemas() {
        OpenDataService service = retrofit.create(OpenDataService.class);
        Call<List<eneas.instantapp.dto.Schema>> call = service.getSchemas(); // Obtnego los mapeados para el repositorio dado
        List<Schema> result = new ArrayList<>();
        try {
            Response<List<eneas.instantapp.dto.Schema>> callout = call.execute();
            for ( eneas.instantapp.dto.Schema schemaDto : callout.body() ) {
                result.add(schemaDto.getSchemaDaoCopy());
            }
        } catch(IOException e) {
            e.printStackTrace();
            // YourInstantApplication.nonCriticalError("I/O Exception");

        }

        return result;
    }

    public static List<Schema> getSchemasForRepository(Integer repo_id) throws IOException {

        OpenDataService service = retrofit.create(OpenDataService.class);
        Call<List<Mapping>> call = service.getMappings(repo_id); // Obtnego los mapeados para el repositorio dado

        List<Mapping> mappings = null;
        List<Schema> schemas = new ArrayList<Schema>();
        SchemaRepository schemaRepository = new SchemaRepository();

        Response<List<Mapping>> callout = call.execute();
        mappings = callout.body();
        for(Mapping mapping : mappings) {
            schemas.add(schemaRepository.getSchemaById(mapping.getSchema().longValue()));


        }

        return schemas;
    }

    public static ListFields<Field> getSchema(long id) throws IOException {
        OpenDataService service = retrofit.create(OpenDataService.class);
        Call<ListFields<Field>> call = service.getAttributes(id);
        return call.execute().body();
    }


    /**
     *
     *        id[0] = repo
     *        id[1] = schema
     *              o
     *        id[0] = dataset
     * @return
     */
    public static List<Object> getDataset(Long... id) throws IOException {
        OpenDataService service = retrofit.create(OpenDataService.class);

        Call<Object> call = (id.length > 1) ? service.getDataset(id[0], id[1]) : service.getDataset(id[0]);

        return (List<Object>)call.execute().body();

    }

    public static List<AbstractModel> getListAbstractModelUsingRepoAndSchema(long idRepo, long idSchema) throws IOException {
        List<Object> dataset = RestService.getDataset(idRepo, idSchema);
        return getListAbstractModel(idSchema, dataset );
    }

    public static List<AbstractModel> getListAbstractModelUsingSchemaAndDataset(long idSchema, long idDataset ) throws IOException {
        List<Object> dataset = RestService.getDataset(idDataset);
        return getListAbstractModel(idSchema, dataset );
    }

    private static List<AbstractModel> getListAbstractModel(long idSchema, List<Object> dataset ) throws NullPointerException, IOException {

            ListFields<Field> fields = RestService.getSchema(idSchema);
            List<AbstractModel> listAbstractModel = new ArrayList<AbstractModel>();
            for(Object data : dataset) { // cada elemento del dataset (por ej. cada restaurante)
                AbstractModel abstractModel = new AbstractModel();
                LinkedTreeMap data_typed = (LinkedTreeMap) data; // cada elemento tiene una lista de atributos
                /** obtengo los campos clave a partir del esquema **/
                abstractModel.setId((String)data_typed.get(fields.getField_id()));
                try {
                    List<String> geolocation_raw = (List<String>) data_typed.get(fields.getField_geolocation());
                    List<Double> geolocation = new ArrayList<Double>();
                    geolocation.add(Double.valueOf(geolocation_raw.get(0)));
                    geolocation.add(Double.valueOf(geolocation_raw.get(1)));
                    abstractModel.setGeolocation(geolocation);
                } catch(Exception e) { };
                abstractModel.setName((String) data_typed.get(fields.getField_title()));
                /****/
                /** campos ordinarios **/
                Hashtable<String, SchemaValue> vars = new Hashtable<String, SchemaValue>();
                for(Field field : fields.getOrdinary_fields()) {
                    vars.put(field.getField(), new SchemaValue(field.getType(), (String)data_typed.get(field.getField())));
                }
                abstractModel.setVars(vars);
                listAbstractModel.add(abstractModel);
            }


            return listAbstractModel;


    }

    public static List<CategoryEntity> getCategories() throws IOException {
        OpenDataService service = retrofit.create(OpenDataService.class);
        Call<List<CategoryEntity>> call = service.getCategories();
        return call.execute().body();
    }

    public static List<Repo> getRepositories() {
        OpenDataService service = retrofit.create(OpenDataService.class);
        Call<List<eneas.instantapp.dto.Repo>> call = service.getRepos();

        List<Repo> result = new ArrayList<>();

        try {
            List<eneas.instantapp.dto.Repo> list_repo_dto = call.execute().body();
            for (eneas.instantapp.dto.Repo repo : list_repo_dto) {
                eneas.instantapp.dao.Repo repo_dao = new eneas.instantapp.dao.Repo();
                repo_dao.setId(repo.getId());
                repo_dao.setGeo_lat(Float.valueOf(repo.getGeoLat()));
                repo_dao.setGeo_long(Float.valueOf(repo.getGeoLong()));
                repo_dao.setKeywords((new Gson()).toJson(repo.getKeywords(), List.class));
                repo_dao.setName(repo.getName());
                repo_dao.setPlace(repo.getPlace());
                repo_dao.setTimestamp(repo.getLastModified().intValue());
                result.add(repo_dao);
            }
        } catch (IOException e) {
            return null;
        }

        return result;

    }
}
