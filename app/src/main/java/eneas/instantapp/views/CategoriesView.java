package eneas.instantapp.views;

import android.view.View;

import butterknife.OnClick;
import eneas.instantapp.R;

/**
 * Created by Eneas on 27/08/2016.
 */
public interface CategoriesView {
    void onClickCheckButton(View v);
}
