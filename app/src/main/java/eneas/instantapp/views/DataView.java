package eneas.instantapp.views;

import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import butterknife.OnClick;
import eneas.instantapp.R;
import eneas.instantapp.dao.Repo;
import eneas.instantapp.dao.Schema;

/**
 * Created by Eneas on 27/08/2016.
 */
public interface DataView {
    void onClickCheckButton(View v);

    void onMapReady(GoogleMap googleMap);

    void onListFragmentInteraction(Repo repo);

    void onListFragmentInteraction(Schema schema);

    void onMapClick(LatLng latLng);

    boolean onMarkerClick(Marker marker);
}
