package eneas.instantapp.views;

/**
 * Created by Eneas on 21/06/2016.
 */
public interface MainView {
    void enable2Step();
    void enable3Step();
    void enable4Step();
    void enable5Step();
    public void finalStep();
    public void navigateToDataActivity(boolean multipoint);
    public int showLocationInput();
    public void  restart();
    void populateNavigationDrawer();

    void launchAppGenerator(Long id, String app_data);

}
